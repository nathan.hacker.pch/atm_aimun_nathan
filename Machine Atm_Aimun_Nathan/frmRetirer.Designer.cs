﻿namespace Machine_Atm_Aimun_Nathan
{
    partial class frmRetirer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnExit = new System.Windows.Forms.Button();
            this.tbxEntrerMontant = new System.Windows.Forms.TextBox();
            this.lblDemandeRetirer = new System.Windows.Forms.Label();
            this.lblInfo = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(698, 403);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 0;
            this.btnExit.Text = "exit";
            this.btnExit.UseVisualStyleBackColor = true;
            // 
            // tbxEntrerMontant
            // 
            this.tbxEntrerMontant.Location = new System.Drawing.Point(246, 114);
            this.tbxEntrerMontant.Name = "tbxEntrerMontant";
            this.tbxEntrerMontant.Size = new System.Drawing.Size(100, 20);
            this.tbxEntrerMontant.TabIndex = 1;
            this.tbxEntrerMontant.TextChanged += new System.EventHandler(this.tbxEntrerMontant_TextChanged);
            // 
            // lblDemandeRetirer
            // 
            this.lblDemandeRetirer.AutoSize = true;
            this.lblDemandeRetirer.Location = new System.Drawing.Point(141, 114);
            this.lblDemandeRetirer.Name = "lblDemandeRetirer";
            this.lblDemandeRetirer.Size = new System.Drawing.Size(40, 13);
            this.lblDemandeRetirer.TabIndex = 2;
            this.lblDemandeRetirer.Text = "Retirez";
            // 
            // lblInfo
            // 
            this.lblInfo.AutoSize = true;
            this.lblInfo.Location = new System.Drawing.Point(182, 158);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(164, 26);
            this.lblInfo.TabIndex = 3;
            this.lblInfo.Text = "attention, faut pas retirer plus que\r\n le montant que vous avez.";
            // 
            // frmRetirer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.lblInfo);
            this.Controls.Add(this.lblDemandeRetirer);
            this.Controls.Add(this.tbxEntrerMontant);
            this.Controls.Add(this.btnExit);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "frmRetirer";
            this.Text = "Retirer";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.TextBox tbxEntrerMontant;
        private System.Windows.Forms.Label lblDemandeRetirer;
        private System.Windows.Forms.Label lblInfo;
    }
}